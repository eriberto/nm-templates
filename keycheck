#!/usr/bin/python3

# Copyright (C) 2017 Enrico Zini <enrico@debian.org> and others
# Copyright (C) 2003-2007 Joerg Jaspert <joerg@debian.org> and others
# This little script is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2.
#
# On Debian systems, the complete text of the GNU General Public
# License can be found in /usr/share/common-licenses/GPL-2 file.
#
# This little (and maybe bad) script is used by me (and maybe others) to
# check keys from NM's.
#
# First it syncs local copies of the debian-keyring with keyring.d.o
# (the keyring package is too old) and then it downloads the
# key of the NM from a keyserver in the local nm.gpg file.
#
# After that it shows the key and all signatures made by existing Debian
# Developers.
#
# Finally, it checks to make sure that the key has encryption and
# signature capabilities, and will continue to have them one month
# into the future.
#
# ~/debian/keyring.debian.org/keyrings/ will be created if it doesn't exist.
#
# Usage:
# give it one option, the keyid.

import argparse
import logging
import os
import sys
import subprocess
import re
import locale
import datetime
import time

log = logging.getLogger()


def choose_english_locale():
    """
    Pick an English locale among the avalable ones
    """
    res = subprocess.run(["locale", "-a"], stdout=subprocess.PIPE, check=True, universal_newlines=True)
    for name in res.stdout.splitlines():
        if re.match(r"^en_.+utf8", name, re.I):
            return name.strip()
    return "C"


class KeyInfo:
    def __init__(self, lines):
        self.records = [line.split(":") for line in lines]
        self.rejected = False

    def filter(self, type):
        for r in self.records:
            if r[0] != type: continue
            yield r

    def check_fpr_length(self):
        for rec in self.filter("fpr"):
            if len(rec[9]) == 32:
                log.warn(
                    "Warning: It looks like this key is an version 3 GPG key. This is bad.\n"
                    "This is not accepted for the NM ID Step. Please doublecheck and then\n"
                    "get your applicant to send you a correct key if this is script isnt wrong."
                )
                self.rejected = True
        log.info("Key is OpenPGP version 4 or greater.")

    def check_pubkey_size(self):
        for rec in self.filter("pub"):
            keysize = int(rec[2])
            if keysize >= 4096:
                log.info("Key has %d bits.", keysize)
            elif keysize >= 2048:
                log.warn("Key has only %d bits.  Please explain why this key size is used\n"
                        "(see [KM] for details).\n"
                        "[KM] https://lists.debian.org/debian-devel-announce/2010/09/msg00003.html", keysize)
            else:
                log.warn("Key has only %d bits.  This is not acceptable.", keysize)
                self.rejected = True

    def check_key_algo(self):
        for rec in self.filter("pub"):
            algo = int(rec[3])
            if algo == 1:
                pass
            elif algo == 17:
                log.warn("This is a DSA key.  This might need an explanation (see [KM] for details).\n"
                        "[KM] https://lists.debian.org/debian-devel-announce/2010/09/msg00003.html")
                self.rejected = True
            else:
                log.warn("Unknown key algorithm %d", algo)
                self.rejected = True

    def check_expiration(self, keyflag, targdate):
        """
        Check whether the key details show a valid usage flag for a given
        future date

        Based on the awk script in keycheck.sh by Daniel Kahn Gillmor <dkg@fifthhorseman.net>

        keyflag: which flag are we looking for? See https://tools.ietf.org/html/rfc4880#section-5.2.3.21
                 gpg supports at least: a (authentication), e (encryption), s (signing), c (certification)
        targdate: unix timestamp of date that we care about
        """
        priflags = None
        prifpr = None
        subfound = False
        expires = None

        for rec in self.records:
            if rec[1] == "r": continue

            if rec[0] == "pub":
                exp = int(rec[6]) if rec[6] else None
                priflags = rec[11]
                prifpr = rec[4]
                if exp is not None and keyflag in priflags:
                    if expires is None or expires < exp:
                        expires = exp
            elif rec[0] == "sub" and keyflag in rec[11]:
                exp = int(rec[6]) if rec[6] else None
                if exp is not None:
                    if expires is None or expires < exp:
                        expires = exp
                subfound = True


        if keyflag not in priflags and not subfound:
            log.warn("No valid '%s' usage flag set on key %s", keyflag, prifpr)
            self.rejected = True
            return

        if not expires:
            log.info("Valid '%s' flag, no expiration", keyflag)
        elif expires > targdate:
            dt = datetime.datetime.utcfromtimestamp(expires)
            log.info("Valid '%s' flag, expires %s", keyflag, dt.strftime("%cUTC"))
        else:
            dt = datetime.datetime.utcfromtimestamp(expires)
            log.warn("Valid '%s' flag, but it expires %s", keyflag, dt.strftime("%cUTC"))
            log.warn("This is too soon. Please ask the applicant to extend the lifetime of their key.")
            self.rejected = True



def main():
    debhome = os.environ.get("DEBHOME")
    if debhome is None:
        debhome = os.path.expanduser("~/debian")
    default_keyrings = os.path.join(debhome, "keyring.debian.org/keyrings")

    parser = argparse.ArgumentParser(
            description="Check keys for prospective Debian members",
        )
    parser.add_argument("--verbose", action="store_true", help="enable verbose output")
    parser.add_argument("--debug", action="store_true", help="enable debugging output")
    parser.add_argument("--keyrings", default=default_keyrings, help="where to rsync debian keyrings and keep nm.gpg")
    parser.add_argument("--keyserver", default="keyserver.ubuntu.com", help="which keyserver to use")
    parser.add_argument("--nonet", "-n", action="store_true", help="don't try to access the network")
    parser.add_argument("--keep", "-k", action="store_true", help="keep nm.gpg after check. nm.gpg holds all keys you checked with this script.")
    parser.add_argument("keyid", help="key id to check")
    args = parser.parse_args()

    FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
    if args.debug:
        verbose = True
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
    elif args.verbose:
        verbose = True
        logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)
    else:
        verbose = False
        logging.basicConfig(level=logging.WARN, stream=sys.stderr, format=FORMAT)

    keyring_file = os.path.join(args.keyrings, "nm.gpg")

    # The options for the gpg call in this script.
    # Contains only options used in ALL gpg calls.
    GPG_CMD=["gpg", "-q", "--no-options", "--no-default-keyring",
             "--no-auto-check-trustdb", "--keyring",
             keyring_file, "--trust-model", "always", '--keyserver-options=no-self-sigs-only']

    # Try to ensure that we can use the output in an english report
    name = choose_english_locale()
    os.environ["LC_ALL"] = name
    os.environ["LANGUAGE"] = name
    locale.setlocale(locale.LC_ALL, name)

    # Get keyrings and key
    os.makedirs(args.keyrings, exist_ok=True)
    if args.nonet:
        res = subprocess.run(["gpg", "--export", "-a", args.keyid], stdout=subprocess.PIPE, check=True)
        subprocess.run(GPG_CMD + ["--import"], input=res.stdout, check=True)
    else:
        subprocess.run(["rsync", "-qcltz", "--block-size=8192", "--partial",
            "--progress", "--exclude=emeritus-*", "--exclude=removed-*",
            "keyring.debian.org::keyrings/keyrings/*",
            os.path.join(args.keyrings, ".")], check=True)
        subprocess.run(GPG_CMD + ["--keyserver=" + args.keyserver, "--recv-keys", args.keyid])

    # Check key

    subprocess.run(GPG_CMD + ["-v", "--with-fingerprint",
        "--keyring", os.path.join(args.keyrings, "debian-keyring.gpg"),
        "--keyring", os.path.join(args.keyrings, "debian-nonupload.gpg"),
        "--check-sigs", args.keyid], check=True)

    warnings = []

    res = subprocess.run(GPG_CMD + ["--with-colons", "--with-fingerprint", "--list-keys", args.keyid],
            stdout=subprocess.PIPE, check=True, universal_newlines=True)
    key_info = KeyInfo(res.stdout.splitlines())

    key_info.check_fpr_length()
    key_info.check_pubkey_size()
    key_info.check_key_algo()

    # we want to make sure that there will be usable, valid keys three months in the future:
    cutoff = time.time() + 86400 * 30 * 3
    key_info.check_expiration("e", cutoff)
    key_info.check_expiration("s", cutoff)

    # Clean up
    if not args.keep:
        os.unlink(keyring_file)

    if key_info.rejected:
        print("#########################################################################")
        print("### There are problems with the key that might make it unusable for   ###")
        print("### inclusion in the Debian keyring or usage with Debian's LDAP       ###")
        print("### directory.  If unsure, please get in touch with the NM front-desk ###")
        print("### nm@debian.org to resolve these problems.                          ###")
        print("### (Please do not include this notice in the AM report.)             ###")
        print("#########################################################################")
        sys.exit(1)

if __name__ == "__main__":
    main()
