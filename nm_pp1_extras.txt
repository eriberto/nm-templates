
 * Extra P&P1 questions

This is an archive of extra P&P1 questions that can be used by AMs if they feel
they should investigate a bit more.

The idea is that it should be obvious to see whether an applicant already knows
such things, for example by looking at https://wiki.debian.org/DDPortfolio
or minechangelogs https://lists.debian.org/debian-newmaint/2009/08/msg00002.html

But in those cases where there is not much visible activity, yet there are
reasons not to tell the applicant to gather some more experience then come
back, one can find these questions useful.

Philosophy
----------

PH0. First, please explain the key points of the Social Contract and
     the DFSG _in your own words_. Also, describe what you think about
     these documents. Please don't just rephrase each point of the
     Social Contract and the DFSG, but try to summarize it as a whole.

PH8. Donald Knuth, author of TeX, insists that no-one has the right to
     modify the source code of TeX, and that any changes must be made using
     "change files" (a sort of patch file). Is this allowed for a program
     for the main component of Debian?

PH9. The GNU Free Documentation License (GFDL) has been heavily
     discussed on debian-legal in the past, leading to a GR [6].
     Please summarize the problems Debian has with the GFDL, and what
     you think about the GR outcome.

     [6] https://www.debian.org/vote/2006/vote_001

PHb. An upstream released the first version of his program under GPL-2+
     license. A year later he decided to release the version 2.0 of the
     same program under BSD-3-Clause. Is there any licensing violation
     here? Can this software be packaged in Debian?
